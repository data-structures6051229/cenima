import java.util.Scanner;

public class Ticket {
    static Scanner kb = new Scanner(System.in);
    static String pay,con;
    private static void showWelcome() {//แสดง welcome
        System.out.println("welcome to Theater");
    }

    private static boolean inputSee(String name) {//เลือกว่าจะดูอะไรกอ่นระหว่าง movie or cinema
        System.out.print("welcome "+name+" input 1 to see Movie input 2 to see Cinema : ");
        int see = kb.nextInt(); 
        System.out.println("----------------------------------------------------------------");
        return see == 1;
        
    }
    private static void showPay(){//แสดงช่องทางจ่ายเงิน และเลือกช่องทางจ่ายเงิน
        System.out.println("input how to pay 1.cash 2.moblie banking");
        System.out.print("input num : ");
        pay = kb.next();
        pay = (pay.equals("1")) ? "cash" : "mobile banking";
        System.out.println("----------------------------------------------------------------");
    }
    private static boolean Nextcustomer(){
        System.out.print("Confirm or cancle ?(yes,no) : ");
        con = kb.next();
        return con.equals("yes");
    }
    public static void main(String[] args) { 
        Movie movie = new Movie();
        Cinema cinema = new Cinema();
        User user = new User();
        while(true){
            showWelcome();
            user.inputInfo();
            if(inputSee(user.getName())){
                movie.showlistMovie();
                cinema.showListCinema();
            }else{
                cinema.showListCinema();
                movie.showlistMovie();
            }
            cinema.inputTime();
            cinema.seat.seat();
            if(user.member()){
                cinema.seat.discount();
            }
            cinema.seat.showPrice();
             
            showPay();
            if(Nextcustomer()){
                System.out.println("complete payment");
                System.out.println("==============================================================");
                System.out.println(" "+user.getName()+" "+user.getTel()+" "+user.getEmail());
                System.out.println(" Cinema name : "+cinema.getCinema());
                System.out.println("------Theater number : 1");
                System.out.println("------Movie name : "+movie.getMovie()+" "+movie.getSub());
                System.out.print(" Time : "+cinema.getTime()+"        ");
                System.out.print(" Seat : ");
                for(int i=0;i<cinema.seat.IDSeat.length;i++){
                    System.out.print(cinema.seat.IDSeat[i]+" ");
                }
                System.out.println();
                System.out.println(" Total : "+cinema.seat.getPrice());
                System.out.println(" show pay : "+pay);
                System.out.println("==============================================================");
            }else{
                cinema.seat.reListSeat();
            }
            }
        
    }
}
